#!/bin/bash

function yay_install {
    cd /opt || exit
    git clone https://aur.archlinux.org/yay.git || echo "the yay git repo could not be cloned!"
    sudo chown -R "$USER":users ./yay
    cd yay || exit 1
    yes | makepkg -si
    cd .. || exit 1
    sudo rm -rf yay || echo "couldn't remove '/opt/yay' directory."
}

function clean_up {
    yes | yay -Syua
    yes | yay -Yc
    yes | yay -Syu
    yes | yay -Scc
    yes | sudo pacman -Scc
}

#intstall yay AUR helper 
yay -Syua --noconfirm || yay_install
clean_up
